"""
Fibonacci Sequence Generator (Iterative Approach)

This script provides a function to generate Fibonacci numbers iteratively,
adhering to PEP 8 guidelines and best practices.

Functions:
    fibonacci_iterative(n: int) -> int:
        Compute the nth Fibonacci number iteratively.

    main():
        Demonstrate the usage of the Fibonacci function.
"""

def fibonacci_iterative(n: int) -> int:
    """
    Compute the nth Fibonacci number iteratively.

    Args:
        n (int): The position in the Fibonacci sequence.

    Returns:
        int: The nth Fibonacci number.

    Raises:
        ValueError: If n is not a positive integer.
    """
    if n <= 0:
        raise ValueError("n must be a positive integer")

    a, b = 0, 1
    for _ in range(1, n):
        a, b = b, a + b
    return a


def main():
    """
    Main function to demonstrate the usage of the Fibonacci function.
    """
    try:
        n = 10
        print(f"The {n}th Fibonacci number (iterative):{fibonacci_iterative(n)}")
    except ValueError as e:
        print(f"Error: {e}")


if __name__ == "__main__":
    main()
